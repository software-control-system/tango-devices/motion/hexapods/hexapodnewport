static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Motion/HexapodNewport/src/ClassFactory.cpp,v 1.1 2010-01-05 12:57:36 olivierroux Exp $";
//+=============================================================================
//
// file :        ClassFactory.cpp
//
// description : C++ source for the class_factory method of the DServer
//               device class. This method is responsible to create
//               all class singletin for a device server. It is called
//               at device server startup
//
// project :     TANGO Device Server
//
// $Author: olivierroux $
//
// $Revision: 1.1 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>
#include <HexapodNewportClass.h>

/**
 *	Create HexapodNewportClass singleton and store it in DServer object.
 */

void Tango::DServer::class_factory()
{

	add_class(HexapodNewport_ns::HexapodNewportClass::init("HexapodNewport"));

}
