/**
 * Device Class Identification:
 * 
 * Class Name   :	HexapodNewport
 * Contact      :	jean.coquet@synchrotron-soleil.fr
 * Class Family :	Motion
 * Platform     :	All Platforms
 * Bus          :	Ethernet
 * Manufacturer :	Newport
 * Reference    :	HXP
 */
