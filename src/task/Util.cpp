#include "Util.h"

void post_msg( yat4tango::DeviceTask & t, size_t msg_id, size_t timeout_ms, bool wait )
{
	yat::Message* msg = 0;
	try
	{
		msg = yat::Message::allocate(msg_id, DEFAULT_MSG_PRIORITY, wait);
	}
	catch( yat::Exception& ex )
	{
		RETHROW_YAT_ERROR(ex,
				"OUT_OF_MEMORY",
				"Out of memory could not allocate msg",
				"Util::post_msg");
	}
  catch(std::bad_alloc)
	{
		THROW_YAT_ERROR(
        "OUT_OF_MEMORY",
				"Out of memory could not allocate msg",
				"Util::post_msg");
	}

	if (wait)
  {
	  try
	  {
			t.wait_msg_handled( msg, timeout_ms );
	  }
	  catch( yat::Exception& ex )
	  {
		  RETHROW_YAT_ERROR(ex,
				  "SOFTWARE_FAILURE",
				  "Unable to wait for a message to be handled",
				  "Util::post_msg");
	  }
  }
  else
  {
	  try
	  {
      t.post( msg, timeout_ms );
    }
	  catch( yat::Exception& ex )
	  {
		  RETHROW_YAT_ERROR(ex,
				  "SOFTWARE_FAILURE",
				  "Unable to post a message",
				  "Util::post_msg");
	  }
  }

}
