//////////////////////////////////////////////////////////////////////
//
// Projet : BT500 server
// StringTokenizer.cpp: implementation of the string tokenizer class.
// cette classe offre 1 service de réduction d'1 chaine avec séparateurs en tokens
//
//////////////////////////////////////////////////////////////////////

#include "StringTokenizer.h"

//- Ctor with initialisation---------------------------------------------
StringTokenizer::StringTokenizer(const std::string _delim,
                                 const std::string & _str_in) : 
                            delimiters(_delim)
{
  this->tokenize(_str_in);
}

//- Ctor without initialisation------------------------------------------
StringTokenizer::StringTokenizer(const std::string _delim) : 
                            delimiters(_delim)
{
  //- noop Ctor
}

//- Dtor ----------------------------------------------------------------
StringTokenizer::~StringTokenizer()
{
  //- noop Dtor
}

//- parse string and cut in tokens ---------------------------------------
void StringTokenizer::tokenize(const std::string & str_in) 
                              throw (Tango::DevFailed)
{
  std::string::size_type beg, end;
  try
  {

    if(token.size() > 0)
      token.clear();

    beg = str_in.find_first_not_of(this->delimiters);
    end = str_in.find_first_of(this->delimiters, beg);

    while (beg != std::string::npos)
    {
      token.push_back(str_in.substr(beg, (end - beg)));
      beg = end;
      if(beg == std::string::npos)
        break;
      end = str_in.find_first_of( this->delimiters, ++beg);
    }
  }
  catch (...)
  {
    THROW_DEVFAILED(_CPTC("SOFTWARE_ERROR"),
                    _CPTC("error trying to tokenize"),
                    _CPTC("StringTokenizer::tokenize"));		
  }
}

