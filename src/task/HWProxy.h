
//- Project : HexapodNewport
//- file : HWProxy.h
//- threaded reading of the HW


#ifndef __HW_PROXY_H__
#define __HW_PROXY_H__

#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include <yat/time/Timer.h>
#include <yat/utils/String.h>

#include <yat/network/ClientSocket.h>

namespace HexapodNewport_ns
{
// ============================================================================
// some defines enums and constants
// ============================================================================
//- startup and communication errors
const size_t MAX_COM_RETRIES = 20;
const size_t MAX_COMMAND_RETRIES = 5;

// ============================================================================
#define NB_VIRTUAL_AXES 6
#define NB_PHYSICAL_AXES 6
// ============================================================================
// ============================================================================
#define HWP_POSIT_RELATIVE 1
#define HWP_POSIT_ABSOLUTE 2
// ============================================================================

typedef enum
{
    HWP_UNKNOWN_ERROR        = 0,
    HWP_INITIALIZATION_ERROR,
    HWP_COMMUNICATION_ERROR,
    HWP_COMMAND_ERROR,
    HWP_HARDWARE_ERROR,
    HWP_SOFTWARE_ERROR,
    HWP_NO_ERROR
} ComState;


//- the status strings for ComState
const size_t HWP_STATE_MAX_SIZE = 7;
static const std::string hw_state_str[HWP_STATE_MAX_SIZE] ={
    "Unknown Error",
    "Initialisation Error",
    "Communication Error",
    "Command Error",
    "Hardware Error",
    "Software Error",
    "Communication Running"
};

typedef enum
{
    X        = 0,
    Y,
    Z,
    U,
    V,
    W
} VirtualAxisName;


// ========================================================
//---------------------------------------------------------  
//- the YAT user messages 
const size_t EXEC_LOW_LEVEL_MSG      = yat::FIRST_USER_MSG + 1000;
const size_t GET_POSITION_VALUES     = yat::FIRST_USER_MSG + 1001;
const size_t GET_BASE_CS_VALUES      = yat::FIRST_USER_MSG + 1002;
const size_t GET_WORK_CS_VALUES      = yat::FIRST_USER_MSG + 1003;
const size_t GET_TOOL_CS_VALUES      = yat::FIRST_USER_MSG + 1004;
const size_t GET_GROUP_STATUS        = yat::FIRST_USER_MSG + 1005;

const size_t HXP_SET_POSITIONS       = yat::FIRST_USER_MSG + 1006;
const size_t SET_BASE_CS_VALUES      = yat::FIRST_USER_MSG + 1007;
const size_t SET_WORK_CS_VALUES      = yat::FIRST_USER_MSG + 1008;
const size_t SET_TOOL_CS_VALUES      = yat::FIRST_USER_MSG + 1009;

const size_t GROUP_INITIALIZE        = yat::FIRST_USER_MSG + 1010;
const size_t GROUP_HOME_SEARCH       = yat::FIRST_USER_MSG + 1011;
const size_t GROUP_HOME_REF          = yat::FIRST_USER_MSG + 1012;
const size_t GROUP_STOP              = yat::FIRST_USER_MSG + 1013;
const size_t GROUP_KILL              = yat::FIRST_USER_MSG + 1014;
const size_t GROUP_DEFINE_POSITION   = yat::FIRST_USER_MSG + 1015;
const size_t GROUP_REFERENCING_START = yat::FIRST_USER_MSG + 1016;
const size_t GROUP_REFERENCING_EXEC  = yat::FIRST_USER_MSG + 1017;



//- structures for use with messages service to write values in the BT500
//- the structure to write a string 

typedef struct LowLevelMsg
{
    std::string cmd;
} LowLevelMsg;

typedef struct
{
    double positions [NB_VIRTUAL_AXES];
} positions_t;

typedef struct
{
    double position;
} position_t;


//------------------------------------------------------------------------
//- HWProxy Class
//- read the HW 
//------------------------------------------------------------------------

class HWProxy : public yat4tango::DeviceTask
{
public:

    //- the configuration structure

    typedef struct Config
    {
        //- members
        std::string url;
        size_t port;
		int socket_snd_tmo;
		int socket_rcv_tmo;			
        size_t task_msg_tmo;		
        size_t task_msg_periodic;
        std::string group_name;
        int positioning_type;
        bool is_linear_trajectory;
        double linear_velocity;
        bool is_rotational_trajectory;
        double rotational_velocity;        
        //- Ctor -------------
        Config ();
        //- Ctor -------------
        Config ( const Config & _src);
        //- Ctor -------------
        Config (std::string url );

        //- operator = -------
        void operator = (const Config & src);
    } Config;

    //- Constructeur/destructeur
    HWProxy (Tango::DeviceImpl * _host_device,
            Config & conf);

    virtual ~HWProxy ();

    //- the number of communication successfully done
    unsigned long get_com_error (void)
    {
        return m_com_error;
    };
    
    //- the number of communication errors
    unsigned long get_com_success (void)
    {
        return m_com_success;
    };
    
    //- the number of commands successfully done
    unsigned long get_command_error (void)
    {
        return m_command_error;
    };
    
    //- the number of command errors
    unsigned long get_command_success (void)
    {
        return m_command_ok;
    };
    
    //- the state and status of communication
    ComState get_m_com_state (void)
    {
        return m_com_state;
    };
    
    std::string get_com_status (void)
    {
        return hw_state_str [m_com_state];
    };
    
    std::string get_last_error (void)
    {
        return m_last_error;
    };
    
    std::string get_hxp_status (void);

    //- access to axes position individually
    double get_virtual_axis_position (VirtualAxisName n);

    //- sends a Low Level Command (do not check the syntax)  to the hard -blocking!
    std::string exec_low_level_command (std::string cmd);

    //- returns the HXP state machnie value;
    Tango::DevState get_hxp_state (void);

	//-  check high velocity limits according to deltaX, deltaY if Linear OR deltaZ or deltaU, deltaV, deltaW if Rotational
	bool check_limit_high_velocity(std::string trajectory_type , double velocity, double array_delta_pos[], unsigned array_size);

	//-  check low velocity limits according to deltaX, deltaY if Linear OR deltaZ or deltaU, deltaV, deltaW if Rotational
	bool check_limit_low_velocity(std::string trajectory_type, double velocity , double array_delta_pos[], unsigned array_size);
	
    //- set the 6 positions at once - use this method and not the message because of state management
    void set_hexapod_positions (positions_t pos, Config conf);

protected:
    //- process_message (implements yat4tango::DeviceTask pure virtual method)
    virtual void process_message (yat::Message& msg)throw (Tango::DevFailed);

private:

    //- get values on the hard : the 6 virtual axes positions and status
    bool read_positions (void);
	
    //- get values on the hard : the 6 virtual axes positions and status
    bool read_hxp_state (void);



    //-----------------------communication stuff---------------------------
    //- get a value on the hardware
    bool write_read (std::string cmd, std::string & response);

    //- write a command (blocking) and forget it
    bool write (std::string cmd);

    //- the configuration
    Config m_conf;
	
    //- the host device 
    Tango::DeviceImpl * m_device;
	
    //- the socket to the HXP for write_read exchanges
    yat::ClientSocket * m_sock_wr;
    
    //- the socket to the HXP for write commands
    yat::ClientSocket * m_sock_w;

    //- au cas ou....
    yat::Mutex m_lock_wr;
    yat::Mutex m_lock_w;
    yat::Mutex m_lock_position;
    
    //- the periodic execution time
    size_t m_periodic_exec_ms;

    //- the state and status stuff
    ComState m_com_state;
    std::string m_com_status;

    std::string m_last_error;

    //- for Com error attribute
    unsigned long m_com_error;

    //- for Com OK attribute
    unsigned long m_com_success;

    //- internal error counter
    unsigned long m_consecutive_com_errors;

    //- for command error (command refused by BT500)
    unsigned long m_command_ok;
    unsigned long m_command_error;
    unsigned long m_consecutive_command_error;


    //- the Hexapod State (the Group State of the internal State Machine)
    int m_group_state;
    int m_request_for_moving;
    yat::Timer m_request_for_moving_timer;

    //- the values read on the hard X Y Z U V W
    double m_positions_read [NB_VIRTUAL_AXES];

    //- the setpoints X Y Z U V W
    double m_positions_setp [NB_VIRTUAL_AXES];
    
    //- the current coordinate system used to write on the Hexapod
    std::string m_name_current_cs;

    //- the firmware version of the hexapod
    std::string m_firmware_version;
} ;

}//- namespace
#endif //- __HW_PROXY_H__
