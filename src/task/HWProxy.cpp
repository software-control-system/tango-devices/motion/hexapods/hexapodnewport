
//- Project : BT500
//- file : HWProxy.cpp

#include <task/HWProxy.h>
#include <math.h>
#include "StringTokenizer.h"
#include "task/Util.h"
#include <iomanip>


namespace HexapodNewport_ns
{
// ============================================================================
// Some defines and constants
// ============================================================================
static const double __NAN__ = ::sqrt(-1.);


// ============================================================================
// Config::Config
// ============================================================================
HWProxy::Config::Config()
{
	url = "Not Initialised";
	port = 5001;
	socket_snd_tmo = 1000;
	socket_rcv_tmo = 1000;		
	group_name = "HEXAPOD";		
	task_msg_tmo = 100;		
	task_msg_periodic = 100;
	positioning_type = 0;
}

// ============================================================================
// Config::Config
// ============================================================================
HWProxy::Config::Config(const Config & _src)
{
	*this = _src;
}

// ============================================================================
// DBConfig::operator =
// ============================================================================
void HWProxy::Config::operator=(const Config & _src)
{
	url = _src.url;
	port = _src.port;
	socket_snd_tmo = _src.socket_snd_tmo;
	socket_rcv_tmo = _src.socket_rcv_tmo;			
	group_name = _src.group_name;
	task_msg_tmo = _src.task_msg_tmo;
	task_msg_periodic = _src.task_msg_periodic;	
    positioning_type = _src.positioning_type;
	is_linear_trajectory = _src.is_linear_trajectory;
	linear_velocity = _src.linear_velocity;
	is_rotational_trajectory = _src.is_rotational_trajectory;
	rotational_velocity = _src.rotational_velocity;
}

// ============================================================================
// HWProxy::HWProxy
// ============================================================================
HWProxy::HWProxy(Tango::DeviceImpl * _host_device, Config & _conf):
yat4tango::DeviceTask(_host_device),
m_conf(_conf),
m_device(_host_device),
m_sock_wr(0),
m_sock_w(0)
{
	DEBUG_STREAM << "HWProxy::HWProxy <- " << std::endl;

	//- yat::Task configure optional msg handling
	enable_timeout_msg(true);
	enable_periodic_msg(true);
	set_timeout_msg_period(m_conf.task_msg_tmo);	
	set_periodic_msg_period(m_conf.task_msg_periodic);

	m_command_ok = 0;
	m_command_error = 0;
	m_consecutive_command_error = 0;

	//- initialize the variables
	for(size_t i = 0;i < NB_VIRTUAL_AXES;i++)
	{
		m_positions_read[i] = m_positions_setp[i] = __NAN__;
	}
}

// ============================================================================
// HWProxy::~HWProxy
// ============================================================================
HWProxy::~HWProxy(void)
{
	DEBUG_STREAM << "HWProxy::~HWProxy <- " << std::endl;

	DEBUG_STREAM << "Disconnecting from peer..." << std::endl;
	m_sock_wr->disconnect();
	m_sock_w->disconnect();

	//- yat internal cooking
	yat::Socket::terminate();

	delete m_sock_wr;
	m_sock_wr = 0;
	delete m_sock_w;
	m_sock_w = 0;
}

// ============================================================================
// HWProxy::process_message
// ============================================================================
void HWProxy::process_message(yat::Message& _msg) throw(Tango::DevFailed)
{
	//- The DeviceTask's lock_ -------------

	DEBUG_STREAM << "HWProxy::handle_message::receiving msg " << _msg.to_string() << std::endl;

	//- handle msg
	switch(_msg.type())
	{
			//- THREAD_INIT =======================
		case yat::TASK_INIT:
		{
			DEBUG_STREAM << "HWProxy::handle_message::THREAD_INIT::thread is starting up" << std::endl;
			//- "initialization" code goes here
			m_last_error = "No Error\n";
			m_request_for_moving = 0;

			//- create the socket
			DEBUG_STREAM << " HWProxy::handle_message::THREAD_INIT try to create socket " << std::endl;
			try
			{
				//- yat internal cooking
				//by default the Socket is in BLOCKING mode (SYNCHRONOUS)
				yat::Socket::init();
				
				m_sock_wr = new yat::ClientSocket();
				m_sock_wr->set_option(yat::Socket::SOCK_OPT_KEEP_ALIVE, 1);
				m_sock_wr->set_option(yat::Socket::SOCK_OPT_NO_DELAY, 1);
				m_sock_wr->set_option(yat::Socket::SOCK_OPT_OTIMEOUT, m_conf.socket_snd_tmo);
				m_sock_wr->set_option(yat::Socket::SOCK_OPT_ITIMEOUT, m_conf.socket_rcv_tmo);

				m_sock_w = new yat::ClientSocket();
				m_sock_w->set_option(yat::Socket::SOCK_OPT_KEEP_ALIVE, 1);
				m_sock_w->set_option(yat::Socket::SOCK_OPT_NO_DELAY, 1);
				m_sock_w->set_option(yat::Socket::SOCK_OPT_OTIMEOUT, m_conf.socket_snd_tmo);
				m_sock_w->set_option(yat::Socket::SOCK_OPT_ITIMEOUT, m_conf.socket_rcv_tmo);

				//- instanciating the Socket (default protocol is yat::Socket::TCP_PROTOCOL)
				yat::Address addr(m_conf.url, m_conf.port);

				//- connect to addr
				DEBUG_STREAM << "Connecting to peer..." << std::endl;
				m_sock_wr->connect(addr);
				m_sock_w->connect(addr);
			}
			catch(const yat::SocketException & se)
			{		
				m_com_state = HWP_INITIALIZATION_ERROR;
				m_last_error = "Initialisation of socket on HXP failed [check cables, power on HXP, restart device]\n";
				ERROR_STREAM << "Error Socket : " <<se.text() << std::endl;	
				for(size_t err = 0;err < se.errors.size();err++)
				{
					ERROR_STREAM << "Err-" << err << "::reason..." << se.errors[err].reason << std::endl;
					ERROR_STREAM << "Err-" << err << "::desc....." << se.errors[err].desc << std::endl;
					ERROR_STREAM << "Err-" << err << "::origin..." << se.errors[err].origin << std::endl;
					ERROR_STREAM << "Err-" << err << "::code....." << se.errors[err].code << std::endl;		
				}
				return;
			}
			
			m_name_current_cs = "Work";
			m_group_state = -1;

			//- Retreiving firmware version
			std::string cmd_firmware_version = "FirmwareVersionGet(char *)";
			std::string reply_firmware_version;
			write_read(cmd_firmware_version, reply_firmware_version);
			size_t pos_sep0 = reply_firmware_version.find(',');
			if(pos_sep0!= std::string::npos)
			{
				//The answer should in the form "0,FIRMWARE_VERSION,EndofAPI"
				size_t pos_sep1 = reply_firmware_version.find(',', pos_sep0+1);
				if(pos_sep1!= std::string::npos)
				{
					m_firmware_version = reply_firmware_version.substr(pos_sep0+1, pos_sep1-pos_sep0-1);
					INFO_STREAM << "Firmware version = " << m_firmware_version << std::endl;
				}
			}

			read_positions();
			read_hxp_state();
			//- initilalze memorized values to something
			for(size_t i = 0;i < NB_VIRTUAL_AXES;i++)
			{
				m_positions_setp[i] = m_positions_read[i];
			}
		}
			break;
			
			//- TASK_EXIT =======================
		case yat::TASK_EXIT:
		{
			DEBUG_STREAM << "HWProxy::handle_message handling TASK_EXIT thread is quitting" << std::endl;

			//- "release" code goes here
		}
			break;
			
			//- TASK_PERIODIC ===================
		case yat::TASK_PERIODIC:
		{
			DEBUG_STREAM << "HWProxy::handle_message handling TASK_PERIODIC msg " << std::endl;
			//- code relative to the task's periodic job goes here
			read_positions();
			read_hxp_state();
		}
			break;

			//- TASK_TIMEOUT ===================
		case yat::TASK_TIMEOUT:
		{
			//- code relative to the task's tmo handling goes here
			ERROR_STREAM << "HWProxy::handle_message handling TASK_TIMEOUT msg" << std::endl;
			m_com_state = HWP_COMMUNICATION_ERROR;
			m_last_error = "HWProxy::handle_message received TASK_TIMEOUT msg\n";
		}
			break;

			//- USER_DEFINED_MSG ================

		case GET_POSITION_VALUES:
		{
			DEBUG_STREAM << "HWProxy::handle_message handling GET_POSITION_VALUES msg" << std::endl;
			read_positions();
		}
			break;
			
		case GET_GROUP_STATUS:
		{
			DEBUG_STREAM << "HWProxy::handle_message handling GET_GROUP_STATUS msg" << std::endl;
			read_hxp_state();
		}
			break;
			
		case GROUP_INITIALIZE:
		{
			DEBUG_STREAM << "HWProxy::handle_message handling GROUP_INITIALIZE msg" << std::endl;
			std::string cmd = "GroupInitialize(" + m_conf.group_name + ")";
			std::string response;
			write_read(cmd, response);
			INFO_STREAM << "HWProxy::handle_message GROUP_INITIALIZE msg cmd = " << cmd << "  response = " << response << std::endl;
		}
			break;
			
		case GROUP_KILL:
		{
			DEBUG_STREAM << "HWProxy::handle_message handling GROUP_KILL msg" << std::endl;
			std::string cmd = "GroupKill(" + m_conf.group_name + ")";
			std::string response;
			write_read(cmd, response);
			INFO_STREAM << "HWProxy::handle_message GROUP_KILL msg cmd = " << cmd << "  response = " << response << std::endl;
		}
			break;
			
		case GROUP_HOME_SEARCH:
		{
			DEBUG_STREAM << "HWProxy::handle_message handling GROUP_HOME_SEARCH msg" << std::endl;
			std::string cmd = "GroupHomeSearch(" + m_conf.group_name + ")";
			std::string response;
			write_read(cmd, response);
			INFO_STREAM << "HWProxy::handle_message GROUP_HOME_SEARCH msg cmd = " << cmd << "  response = " << response << std::endl;
		}
			break;
			
		case GROUP_HOME_REF:
		{
			INFO_STREAM << "HWProxy::handle_message handling GROUP_HOME_REF msg" << std::endl;
			std::string cmd = "GroupKill(" + m_conf.group_name + ")";
			std::string response;
			write_read(cmd, response);
			INFO_STREAM << "HWProxy::handle_message GROUP_KILL msg cmd = " << cmd << "  response = " << response << std::endl;
			cmd = "GroupInitialize(" + m_conf.group_name + ")";
			write_read(cmd, response);
			INFO_STREAM << "HWProxy::handle_message GROUP_INITIALIZE msg cmd = " << cmd << "  response = " << response << std::endl;
			cmd = "GroupHomeSearch(" + m_conf.group_name + ")";
			write_read(cmd, response);
			INFO_STREAM << "HWProxy::handle_message GROUP_HOME_SEARCH msg cmd = " << cmd << "  response = " << response << std::endl;

			INFO_STREAM << "HWProxy::handle_message handling GROUP_HOME_REF msg after GROUP_HOME_SEARCH" << std::endl;
		}
			break;
			
		case GROUP_STOP:
		{
			DEBUG_STREAM << "HWProxy::handle_message handling GROUP_STOP msg" << std::endl;
			std::string cmd = "GroupMoveAbort(" + m_conf.group_name + ")";
			std::string response;
			write_read(cmd, response);
		}
			break;
			
		case GROUP_REFERENCING_START:
		{
			DEBUG_STREAM << "HWProxy::handle_message handling GROUP_REFERENCING_START msg" << std::endl;
			std::string cmd = "GroupReferencingStart(" + m_conf.group_name + ")";
			std::string response;
			write_read(cmd, response);
		}
			break;
			
		case GROUP_REFERENCING_EXEC:
		{
			DEBUG_STREAM << "HWProxy::handle_message handling GROUP_REFERENCING_START msg" << std::endl;
			//- get msg data...
			positions_t * pos = 0;
			_msg.detach_data(pos);
			std::string cmd = "GroupReferencingStart(" + m_conf.group_name + ")";
			std::string response;
			write_read(cmd, response);
		}
			break;
			
		case GROUP_DEFINE_POSITION:
		{
			DEBUG_STREAM << "HWProxy::handle_message handling GROUP_DEFINE_POSITION msg" << std::endl;
			//- get msg data...
			positions_t * pos = 0;
			_msg.detach_data(pos);
			if(pos)
			{
				post_msg(*this, GROUP_KILL, m_conf.task_msg_tmo, true);
				post_msg(*this, GROUP_INITIALIZE, m_conf.task_msg_tmo, true);
				post_msg(*this, GROUP_REFERENCING_START, m_conf.task_msg_tmo, true);
				post_msg(*this, GROUP_REFERENCING_EXEC, m_conf.task_msg_tmo, true, pos);
			}
		}
			break;
			
		case HXP_SET_POSITIONS:
		{
			//- mark the request for the state
			m_request_for_moving = 1;
			m_request_for_moving_timer.restart();

			DEBUG_STREAM << "HWProxy::handle_message handling HXP_SET_POSITIONS msg" << std::endl;

			//- get msg data...
			positions_t * pos = 0;
			_msg.detach_data(pos);
			if(pos)
			{
                yat::AutoMutex<> guard(m_lock_position);
				//- update the internal setpoints m_positions_setp

				std::stringstream cmd;
				cmd << "HexapodMoveAbsolute"
					<<"("
					<< m_conf.group_name
					<< ","
					<< m_name_current_cs;				
					for(size_t i = 0;i < NB_VIRTUAL_AXES;i++)
					{
						m_positions_setp [i] = pos->positions [i];
						cmd << "," << pos->positions [i];
					}				
				cmd << ")"
					<< std::endl;
				write(cmd.str());
			}
		}
			break;
			
		default:
			ERROR_STREAM << "HWProxy::handle_message::unhandled msg type received" << std::endl;
			break;
	} //- switch (_msg.type())
} //- HWProxy::process_message


// ============================================================================
// HWProxy::check_limit_high_velocity
// ============================================================================
bool HWProxy::check_limit_high_velocity(std::string trajectory_type, double velocity, double array_delta_pos [], unsigned array_size)
{
	INFO_STREAM <<"HWProxy::check_limit_high_velocity"<<std::endl;
	bool is_velocity_allowed = false;
    double delta_axis_1 = 0.0;
    double delta_axis_2 = 0.0;
    double delta_axis_3 = 0.0;
	if(trajectory_type == "Line")
	{
		delta_axis_1 = array_delta_pos[0];
		delta_axis_2 = array_delta_pos[1];
		delta_axis_3 = array_delta_pos[2];
		INFO_STREAM<<"delta_x = "<<delta_axis_1<<std::endl;
		INFO_STREAM<<"delta_y = "<<delta_axis_2<<std::endl;
		INFO_STREAM<<"delta_z = "<<delta_axis_3<<std::endl;		
	}
	else
	if(trajectory_type == "Rotation")
	{
		delta_axis_1 = array_delta_pos[3];
		delta_axis_2 = array_delta_pos[4];
		delta_axis_3 = array_delta_pos[5];		
		INFO_STREAM<<"delta_u = "<<delta_axis_1<<std::endl;
		INFO_STREAM<<"delta_v = "<<delta_axis_2<<std::endl;
		INFO_STREAM<<"delta_w = "<<delta_axis_3<<std::endl;		
	}	
	else
	{
		Tango::Except::throw_exception(	"OPERATION_NOT_ALLOWED",
										"cannot check velocity limit in NoVelocity Mode!",
										"HWProxy::check_limit_high_velocity");
	}
	
	std::stringstream cmd("");
	//HexapodMoveIncrementalControlLimitGet (GroupName, CoordinateSystem,	HexapodTrajectoryType, dX, dY, dZ);
	cmd << "HexapodMoveIncrementalControlLimitGet"
		<<"("
		<< m_conf.group_name
		<< ","
		<< m_name_current_cs
		<< ","
		<< trajectory_type
		<< std::setprecision(6)
		<< std::fixed
		<< ","
		<< delta_axis_1
		<< ","
		<< delta_axis_2
		<< ","
		<< delta_axis_3
		<< ","
		<< "double*"
		<< ","
		<< "double*"
		<< ")"
		<< std::endl;			

	//write to the driver
	std::string response;
	if(write_read(cmd.str(), response))
	{
		DEBUG_STREAM <<"response = "<<response<<std::endl;
		std::vector<std::string> vec_lines;
		yat::StringUtil::split( response, ',', &vec_lines, true);
		double hw_max_speed = 0.0;
		double trajectory_percent = 0.0;
		if(vec_lines.size() == 4)//response must be like  "0,speed,percent,EndOfApi"
		{
			hw_max_speed = yat::StringUtil::to_num<double>(vec_lines.at(1));
			INFO_STREAM << "hw_max_speed = " << hw_max_speed << std::endl;			
			trajectory_percent = yat::StringUtil::to_num<double>(vec_lines.at(2));	
			INFO_STREAM << "trajectory_percent = " << trajectory_percent << std::endl;			
		}

		//if 100% trajectory is possible AND our velocity is less than max speed returned by the driver, then it is OK
		if((fabs(trajectory_percent - 1.0)<std::numeric_limits<double>::epsilon()) && (velocity < hw_max_speed))
		{
			is_velocity_allowed = true;
			INFO_STREAM<<"Velocity is allowed "<<std::endl;
		}
		else
		{
			is_velocity_allowed = false;
			INFO_STREAM<<"Velocity is not allowed "<<std::endl;
		}
	}
	return is_velocity_allowed ;
}


// ============================================================================
// HWProxy::check_limit_low_velocity
// ============================================================================
bool HWProxy::check_limit_low_velocity(std::string trajectory_type, double velocity , double array_delta_pos[], unsigned array_size)
{
	INFO_STREAM <<"HWProxy::check_limit_low_velocity"<<std::endl;
	bool is_velocity_allowed = false;
    //- distance = sqrt(dX^2+dY^2+dZ^2)
    double delta_axis_1 = 0.0;
    double delta_axis_2 = 0.0;
    double delta_axis_3 = 0.0;
	if(trajectory_type == "Line")
	{
		delta_axis_1 = array_delta_pos[0];
		delta_axis_2 = array_delta_pos[1];
		delta_axis_3 = array_delta_pos[2];
		INFO_STREAM<<"delta_x = "<<delta_axis_1<<std::endl;
		INFO_STREAM<<"delta_y = "<<delta_axis_2<<std::endl;
		INFO_STREAM<<"delta_z = "<<delta_axis_3<<std::endl;		
	}
	else
	if(trajectory_type == "Rotation")
	{
		delta_axis_1 = array_delta_pos[3];
		delta_axis_2 = array_delta_pos[4];
		delta_axis_3 = array_delta_pos[5];		
		INFO_STREAM<<"delta_u = "<<delta_axis_1<<std::endl;
		INFO_STREAM<<"delta_v = "<<delta_axis_2<<std::endl;
		INFO_STREAM<<"delta_w = "<<delta_axis_3<<std::endl;		
	}	
	else
	{
		Tango::Except::throw_exception(	"OPERATION_NOT_ALLOWED",
										"cannot check velocity limit in NoVelocity Mode!",
										"HWProxy::check_limit_low_velocity");
	}
	
    double distance = std::sqrt(pow(delta_axis_1,2) + pow(delta_axis_2,2) + pow(delta_axis_3,2));
    INFO_STREAM<<"distance (Euclidean) = "<< distance<< std::endl;

    //- The speed lowest limit it empirically appear be a distance/speed = 200 (longest scan 200 seconds: empirically tested)
    if (distance / velocity < 200)
    {
		is_velocity_allowed	 = true;
		INFO_STREAM<<"Velocity is allowed "<<std::endl;
    }
	else
	{
		is_velocity_allowed	 = false;
		INFO_STREAM<<"Velocity is not allowed "<<std::endl;
	}
	
	return is_velocity_allowed;

}
// ============================================================================
// HWProxy::set_hexapod_positions
//- sets the positions X Y Z U V W in the HXP
// ============================================================================
void HWProxy::set_hexapod_positions(positions_t pos, Config conf)
{
	m_conf = conf;
	//- if already moving : throw DevFailed
	if(get_hxp_state() == Tango::MOVING)
	{
		Tango::Except::throw_exception(	"OPERATION_NOT_ALLOWED",
										"cannot request to move, hexapod is already moving",
										"HWProxy::set_hexapod_positions");
	}

	if(m_conf.positioning_type == 0)
	{
		Tango::Except::throw_exception(	"OPERATION_NOT_ALLOWED",
										"cannot request to move, Property PositioningType not defined",
										"HWProxy::set_hexapod_positions");
	}

	//- mark the request for the state
	m_request_for_moving = 1;
	m_request_for_moving_timer.restart();

	DEBUG_STREAM << "HWProxy::set_hexapod_positions <-" << std::endl;
	
	std::stringstream cmd;
	if(m_conf.positioning_type == HWP_POSIT_ABSOLUTE)
	{
        yat::AutoMutex<> guard(m_lock_position);
		INFO_STREAM << "HWProxy::set_hexapod_positions HWP_POSIT_ABSOLUTE" << std::endl;
		//- update the internal setpoints m_positions_setp
		cmd << "HexapodMoveAbsolute"
			<<"("
			<< m_conf.group_name
			<< ","
			<< m_name_current_cs;		
			for(size_t i = 0;i < NB_VIRTUAL_AXES;i++)
			{
				m_positions_setp [i] = pos.positions [i];
				cmd << "," << pos.positions [i];
			}
		cmd << ")"
			<< std::endl;
	}
	else
	{   yat::AutoMutex<> guard(m_lock_position);
		//- exception if request for ROTATION AND TRANSLATION
		bool translation_request = false;
		if(	(m_positions_setp [0] != pos.positions [0]) ||
			(m_positions_setp [1] != pos.positions [1]) ||
			(m_positions_setp [2] != pos.positions [2]))
		{
			INFO_STREAM << "HWProxy::set_hexapod_positions TRANSLATION_REQUEST" << std::endl;
			INFO_STREAM << "[" << m_positions_setp [0] << " : " << pos.positions [0] << "]";
			INFO_STREAM << "[" << m_positions_setp [1] << " : " << pos.positions [1] << "]";
			INFO_STREAM << "[" << m_positions_setp [2] << " : " << pos.positions [2] << "]" << std::endl;
			translation_request = true;
		}
		
		bool rotation_request = false;
		if(	(m_positions_setp [3] != pos.positions [3]) ||
			(m_positions_setp [4] != pos.positions [4]) ||
			(m_positions_setp [5] != pos.positions [5]))
		{
			INFO_STREAM << "HWProxy::set_hexapod_positions ROTATION_REQUEST" << std::endl;
			INFO_STREAM << "[" << m_positions_setp [3] << " : " << pos.positions [3] << "]";
			INFO_STREAM << "[" << m_positions_setp [4] << " : " << pos.positions [4] << "]";
			INFO_STREAM << "[" << m_positions_setp [5] << " : " << pos.positions [5] << "]" << std::endl;
			rotation_request = true;
		}

		if(translation_request && rotation_request)
		{
			m_last_error = "Error : In Relative Move cannot request simultaneous Rotation AND Translation\n";
			ERROR_STREAM << "Error : In Relative Move cannot request simultaneous Rotation AND Translation\n";
			ERROR_STREAM << "---X : [" << m_positions_setp [0] << " : " << pos.positions [0] << "] delta = " << (m_positions_setp [0] - pos.positions [0]);
			ERROR_STREAM << "---Y : [" << m_positions_setp [1] << " : " << pos.positions [1] << "] delta = " << (m_positions_setp [1] - pos.positions [1]);
			ERROR_STREAM << "---Z : [" << m_positions_setp [2] << " : " << pos.positions [2] << "] delta = " << (m_positions_setp [2] - pos.positions [2]);
			ERROR_STREAM << "---U : [" << m_positions_setp [3] << " : " << pos.positions [3] << "] delta = " << (m_positions_setp [3] - pos.positions [3]);
			ERROR_STREAM << "---V : [" << m_positions_setp [4] << " : " << pos.positions [4] << "] delta = " << (m_positions_setp [4] - pos.positions [4]);
			ERROR_STREAM << "---W : [" << m_positions_setp [5] << " : " << pos.positions [5] << "] delta = " << (m_positions_setp [5] - pos.positions [5]) << std::endl;
			//- reset to current positions
			m_positions_setp [0] = m_positions_read [0];
			m_positions_setp [1] = m_positions_read [1];
			m_positions_setp [2] = m_positions_read [2];
			m_positions_setp [3] = m_positions_read [3];
			m_positions_setp [4] = m_positions_read [4];
			m_positions_setp [5] = m_positions_read [5];
			Tango::Except::throw_exception(	"OPERATION_NOT_ALLOWED",
											"In Relative Move cannot request simultaneous Rotation AND Translation",
											"HWProxy::set_hexapod_positions");
		}

        if(translation_request || rotation_request)
        {
            double incremental_move [6];
            
            if(translation_request)
            {
                INFO_STREAM<<"translation_request = true"<<std::endl;
                m_positions_setp [0] = pos.positions [0];
                incremental_move [0] = pos.positions [0] - m_positions_read [0];
                m_positions_setp [1] = pos.positions [1];
                incremental_move [1] = pos.positions [1] - m_positions_read [1];
                m_positions_setp [2] = pos.positions [2];
                incremental_move [2] = pos.positions [2] - m_positions_read [2];
                m_positions_setp [3] = pos.positions [3];
                incremental_move [3] = 0.;
                m_positions_setp [4] = pos.positions [4];
                incremental_move [4] = 0.;
                m_positions_setp [5] = pos.positions [5];
                incremental_move [5] = 0.;
            }
            
            if(rotation_request)
            {
                INFO_STREAM<<"rotation_request = true"<<std::endl;
                m_positions_setp [0] = pos.positions [0];
                incremental_move [0] = 0.;
                m_positions_setp [1] = pos.positions [1];
                incremental_move [1] = 0.;
                m_positions_setp [2] = pos.positions [2];
                incremental_move [2] = 0.;
                m_positions_setp [3] = pos.positions [3];
                incremental_move [3] = pos.positions [3] - m_positions_read [3];
                m_positions_setp [4] = pos.positions [4];
                incremental_move [4] = pos.positions [4] - m_positions_read [4];
                m_positions_setp [5] = pos.positions [5];
                incremental_move [5] = pos.positions [5] - m_positions_read [5];
            }


            INFO_STREAM << "HWProxy::set_hexapod_positions HWP_POSIT_RELATIVE" << std::endl;
            std::string trajectory_type = "NoVelocity";
            double velocity = __NAN__;
            int index_first_virtual_axes = 0;
            if(m_conf.is_linear_trajectory)
            {
                trajectory_type = "Line";
                velocity = m_conf.linear_velocity;
                index_first_virtual_axes = 0;			
            }
            else if(m_conf.is_rotational_trajectory)
            {
                trajectory_type = "Rotation";
                velocity = m_conf.rotational_velocity;
                index_first_virtual_axes = 3;			
            }
            else if(!m_conf.is_linear_trajectory && !m_conf.is_rotational_trajectory)
            {
                trajectory_type = "NoVelocity";
                index_first_virtual_axes = 0;		
            }
            else
            {
                Tango::Except::throw_exception("OPERATION_NOT_ALLOWED",
                                                "Trajectory type (Line , Rotation, NoVelocity) is not correctly set!",
                                                "HWProxy::set_hexapod_positions");			
            }
            
            INFO_STREAM<<"trajectory_type = " <<trajectory_type<<endl;
            //as before !
            if(trajectory_type == "NoVelocity")
            {
                cmd << "HexapodMoveIncremental"
                    <<"("
                    << m_conf.group_name
                    << ","
                    << m_name_current_cs
                    << std::setprecision(6)
                    << std::fixed;
                    for(size_t i = index_first_virtual_axes;i < NB_VIRTUAL_AXES;i++)
                    {
                        
                        cmd << "," << incremental_move [i];
                    }
                cmd << ")"
                    << std::endl;
            }
            else//with velocity
            {			
                //check low velocity
                if (!check_limit_low_velocity(trajectory_type, velocity, incremental_move, 6))
                {
                    Tango::Except::throw_exception("OPERATION_NOT_ALLOWED",
                                                    "Velocity value is lower than limit !",
                                                    "HWProxy::set_hexapod_positions");
                }
                
                //check high velocity
                if (!check_limit_high_velocity(trajectory_type, velocity, incremental_move, 6))
                {
                    Tango::Except::throw_exception("OPERATION_NOT_ALLOWED",
                                                    "Velocity value is higher than limit !",
                                                    "HWProxy::set_hexapod_positions");
                }			
                ////////		
                
                cmd << "HexapodMoveIncrementalControlWithTargetVelocity"
                    <<"("
                    << m_conf.group_name
                    << ","
                    << m_name_current_cs
                    << ","
                    << trajectory_type
                    << std::setprecision(6)
                    << std::fixed;
                    for(size_t i = 0;i < 3 ;i++)//X,Y,Z for linear, u,v,w for rotational, always 3 axies
                    {
                        cmd << "," << incremental_move [index_first_virtual_axes + i];
                    }
                cmd << ","
                    << velocity
                    << ")"
                    << std::endl;			
            }
        }
        else
        {
            INFO_STREAM<<"No translation or Rotation movement Requested !"<<std::endl;
            return;
        }
	}
	
    INFO_STREAM<<"Cmd sent : "<<cmd.str()<<std::endl;
	//write to the driver
	write(cmd.str());
}

// ============================================================================
// HWProxy::get_virtual_axis_position
//- returns a single "virtual axis" position X Y Z U V W
// ============================================================================
double HWProxy::get_virtual_axis_position(VirtualAxisName n)
{
    yat::AutoMutex<> guard(m_lock_position);
	return m_positions_read [n];
}

// ============================================================================
// HWProxy::read_positions
//- read positions on the hard
//- gets the positions on the hard
// ============================================================================
bool HWProxy::read_positions()
{
	DEBUG_STREAM << "HWProxy::read_hardware <-" << std::endl;
	//- case of  device proxy error
	if(m_com_state == HWP_INITIALIZATION_ERROR)
		return false;

	std::string response;
	//- lire les 6 positions de l'hexapod
	//- commande : pour le groupe HEXAPOD de 6 axes virtuels XYZUVW 
	//- GroupPositionCurrentGet (HEXAPOD, double *, double *, double *, double *, double *, double *)
	std::string cmd = "GroupPositionCurrentGet(" + m_conf.group_name + ", double *, double *, double *, double *, double *, double *)";
	if( m_firmware_version.find("XPS-D")!=std::string::npos)
	{
		cmd = "HexapodPositionCurrentGet(" + m_conf.group_name + ", double *, double *, double *, double *, double *, double *)";
	}

	if(write_read(cmd, response))
	{
        yat::AutoMutex<> guard(m_lock_position);
		//- parse the response and put it in the array
		//- response form 0,0.1789513061472,0.02960289503446,0.0603300456024,-0.02644901123962,0.0699330284329,-0.0001644046095955,EndOfAPI
		//- response form cmd status,X,Y,Z,U,V,W,EndOfAPI
		StringTokenizer tok(",", response);
		if(tok.get_number_of_token() == (NB_VIRTUAL_AXES + 2))
		{
			for(size_t i = 0;i < NB_VIRTUAL_AXES;i++)
			{
				m_positions_read[i] = tok.get_token <double> (i + 1);
				DEBUG_STREAM << "HWProxy::read_hardware virtual axis " << i << " = " << m_positions_read[i] << std::endl;
			}
		}
		return true;
	}
	else
	{
		return false;
	}
	return true;
}

// ============================================================================
// HWProxy::read_hxp_state
//- get the haxapod state
// ============================================================================
bool HWProxy::read_hxp_state()
{
	DEBUG_STREAM << "HWProxy::read_hxp_state <-" << std::endl;
	//- case of  device proxy error
	if(m_com_state == HWP_INITIALIZATION_ERROR)
		return false;

	std::string response;
	std::string cmd = "GroupStatusGet(" + m_conf.group_name + ",int *)";
	if(write_read(cmd, response))
	{
		StringTokenizer tok(",", response);
		if(tok.get_number_of_token() <= 2)
		{
			m_com_state = HWP_COMMAND_ERROR;
			m_last_error = "WProxy::read_hardware invalid command [" + cmd + "] response [" + response + "]\n";
		}
		m_group_state = tok.get_token <int> (1);
		return true;
	}
	else
	{
		return false;
	}
	
	return true;
}

// ============================================================================
// HWProxy::get_hxp_state
//- returns the HXP state machine value;
// ============================================================================
Tango::DevState HWProxy::get_hxp_state(void)
{
	static const double REQUEST_FOR_MOVING_TIMEOUT = 200.;
	DEBUG_STREAM << "HWProxy::get_hxp_state <-" << std::endl;

	//- case of  device proxy error
	if(m_com_state == HWP_INITIALIZATION_ERROR)
	{		
		return Tango::FAULT;
	}
	
	if(m_request_for_moving != 0)
	{
		if(m_group_state == -1)
		{
			return Tango::INIT;
		}
	}

	//- NOT INIT
	if((m_group_state >= 0 &&
		m_group_state <= 9) ||
		m_group_state == 41 ||
		m_group_state == 42 ||
		m_group_state == 49 ||
		m_group_state == 50)
	{
		return Tango::INIT;
	}

	//- make sure request for moving is not eternal
	if(	m_request_for_moving != 0 &&
		m_request_for_moving_timer.elapsed_msec() > REQUEST_FOR_MOVING_TIMEOUT)
	{
		m_request_for_moving = 0;
	}


	//- the m_group_state is MOVING    
	if(	m_request_for_moving != 0	||
		m_group_state == 43			||	//- HOMING
		m_group_state == 44			||	//- POSITIONING
		m_group_state == 45			||	//- TRAJECTORY
		m_group_state == 47)			//- JOGGING
	{
		if(m_group_state != 12)
		{
			m_request_for_moving = 0;
		}
		return Tango::MOVING;
	}

	//- ALARM
	if(m_group_state == 40) //- Emergency break
		return Tango::ALARM;

	//- OFF   
	if(m_group_state >= 20 && m_group_state <= 38) //- Disable (motor OFF)
	{
		return Tango::OFF;
	}

	//- STANDBY
	if(	m_group_state >= 10 &&
		m_group_state <= 13 &&
		m_request_for_moving == 0)
	{
		return Tango::STANDBY;
	}

	//- finally if no match found
	return Tango::FAULT;
}

//-----------------------------------------------
//- returns the HXP status machine value;
//- 
//-----------------------------------------------
std::string HWProxy::get_hxp_status(void)
{
	DEBUG_STREAM << "HWProxy::get_hxp_status <-" << std::endl;

	//- case of  device proxy error
	if(m_com_state == HWP_INITIALIZATION_ERROR)
		return "Initialisation error";

	if(m_group_state == -1)
		return std::string("-1  thread en cours d'initialisation");

	std::string response;
	std::stringstream cmd;
	cmd << "GroupStatusStringGet(" << m_group_state << ", char *)";
	write_read(cmd.str(), response);
	cmd.str("");
	cmd << std::endl << "group state value = " << m_group_state << std::endl;
	return response + cmd.str();
}

//-----------------------------------------------
//- write_read
//- send command returns result
//-----------------------------------------------
bool HWProxy::write_read(std::string cmd, std::string & response)
{
	DEBUG_STREAM << "HWProxy::write_read <-" << cmd<<std::endl;
	//- case of  device proxy error
	if(m_com_state == HWP_INITIALIZATION_ERROR)
		return false;

	size_t nb = 0;
	try
	{
		{ //- CRITICAL SECTION
			yat::AutoMutex<> guard(m_lock_wr);
			m_sock_wr->send(cmd);
			nb = m_sock_wr->receive(response);
		}
		
		//- response empty
		if(response.size() <= 0)
		{
			m_com_state = HWP_COMMUNICATION_ERROR;
			m_last_error = "empty response" + cmd + "\n";
			return false;
		}
		
		//- command error
		if(response[0] != '0')
		{
			m_com_state = HWP_COMMAND_ERROR;
			StringTokenizer tok(",", response);
			std::string ret_code = tok.get_token <std::string> (0);

			{ //- CRITICAL SECTION
				yat::AutoMutex<> guard(m_lock_wr);
				m_sock_wr->send(std::string("ErrorStringGet(" + ret_code + ",char*)"));
				nb = m_sock_wr->receive(response);
			}
			
			ERROR_STREAM << "HWProxy::write_read command " << cmd << " refused with message [" << response << "]" << std::endl;
			m_last_error = "command [" + cmd + "] refused with message [" + response + "]\n";
			return false;
		}
		DEBUG_STREAM << "HWProxy::write_read() response = [" << response << "] length = " << nb << std::endl;
	}
	catch(const yat::SocketException & se)
	{	
		m_com_state = HWP_COMMUNICATION_ERROR;
		m_last_error = "communication failed for command " + cmd;
		ERROR_STREAM << "Error Socket : " <<se.text() << std::endl;				
		for(size_t err = 0;err < se.errors.size();err++)
		{
			ERROR_STREAM << "Err-" << err << "::reason..." << se.errors[err].reason << std::endl;
			ERROR_STREAM << "Err-" << err << "::desc....." << se.errors[err].desc << std::endl;
			ERROR_STREAM << "Err-" << err << "::origin..." << se.errors[err].origin << std::endl;
			ERROR_STREAM << "Err-" << err << "::code....." << se.errors[err].code << std::endl;		
		}
		return false;
	}
	catch(...)
	{
		ERROR_STREAM << "HWProxy::write_read()*** Unknown exception caught ***" << std::endl;
		m_com_state = HWP_COMMUNICATION_ERROR;
		m_last_error = "communication failed for command " + cmd;
		return false;
	}
	
	m_com_state = HWP_NO_ERROR;
	return true;
}

//-----------------------------------------------
//- write
//- send a command  (on blocking socket, forget the response)
//-----------------------------------------------
bool HWProxy::write(std::string cmd)
{
	DEBUG_STREAM << "HWProxy::write <-" << std::endl;
	//- case of  device proxy error
	if(m_com_state == HWP_INITIALIZATION_ERROR)
		return false;
	std::string response;
	size_t nb;

	try
	{
		{ //- CRITICAL SECTION
			yat::AutoMutex<> guard(m_lock_w);
			m_sock_w->send(cmd);
			//-       nb  = m_sock_w->receive(response); 
			INFO_STREAM << "HWProxy::write command " << cmd << " response " << response << std::endl;
		}
	}
	catch(const yat::SocketException & se)
	{
		m_com_state = HWP_COMMUNICATION_ERROR;
		m_last_error = "communication failed for command " + cmd;
		ERROR_STREAM << "Error Socket : " <<se.text() << std::endl;				
		for(size_t err = 0;err < se.errors.size();err++)
		{
			ERROR_STREAM << "Err-" << err << "::reason..." << se.errors[err].reason << std::endl;
			ERROR_STREAM << "Err-" << err << "::desc....." << se.errors[err].desc << std::endl;
			ERROR_STREAM << "Err-" << err << "::origin..." << se.errors[err].origin << std::endl;
			ERROR_STREAM << "Err-" << err << "::code....." << se.errors[err].code << std::endl;		
		}
		return false;
	}
	catch(...)
	{
		ERROR_STREAM << "HWProxy::write ()*** Unknown exception caught ***" << std::endl;
		m_com_state = HWP_COMMUNICATION_ERROR;
		m_last_error = "communication failed for command " + cmd;
		return false;
	}
	
	m_com_state = HWP_NO_ERROR;
	INFO_STREAM << "HWProxy::write returning success" << std::endl;
	return true;
}

//-----------------------------------------------
//- exec_low_level_command
//- sends a Low Level Command (do not check the syntax)  to the hard
//-----------------------------------------------
std::string HWProxy::exec_low_level_command(std::string cmd)
{
	DEBUG_STREAM << "HWProxy::exec_low_level_command for command [" << cmd << "\n]" << std::endl;
	std::string response;
	write_read(cmd, response);
	return response;
}

} //- namespace
