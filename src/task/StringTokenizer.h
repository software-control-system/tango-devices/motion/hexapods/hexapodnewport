//////////////////////////////////////////////////////////////////////
//
// Projet : MotorisedInsertion server
// StringTokenizer.h: implementation of the string tokenizer class.
// cette classe offre 1 service de réduction d'1 chaine avec séparateurs en tokens
//
//////////////////////////////////////////////////////////////////////

#ifndef __STRING_TOKENIZER_H__
#define __STRING_TOKENIZER_H__


#include <tango.h>
#include <yat4tango/ExceptionHelper.h>
#include <yat/utils/XString.h>

class StringTokenizer 
{
public :

  //- Ctor with string initialisation
  StringTokenizer(const std::string delims,
                  const std::string & str_in);

  //- Ctor without string initialisation
  StringTokenizer(const std::string delims);

  //- Dtor
  virtual ~StringTokenizer();

  //- feed class with string to tokenize
  void tokenize(const std::string & str_in) throw (Tango::DevFailed);

  //- get the token 
  template<class T> 
  void  get_token(size_t index, T & value) throw (Tango::DevFailed)
  {
    if(index >= token.size())
    {
      THROW_DEVFAILED(_CPTC("DATA_OUT_OF_RANGE"),
                      _CPTC("index out of range"),
                      _CPTC("StringTokenizer::get_token"));		
    }
    try
    {
      value = yat::XString<T>::to_num(token[index], true);
    }
    _HANDLE_YAT_EXCEPTION("yat::XString<T>::to_num", "StringTokenizer::get_token");
  };



  //- call this way : int maval = this->get_token<int> (3);
  template<class T> 
  T get_token(int index) throw (Tango::DevFailed)
  {
    T tmp;
    this->get_token (index, tmp);
    return tmp;
  }


  //- returns the number of tokens
  size_t get_number_of_token(void) {return token.size();}

  //- clear the internal vector 
  void clear (void)
  {
    token.clear ();
  }

private :

  std::vector<std::string> token;
  std::string delimiters;
};

#endif //__STRING_TOKENIZER_H__
